﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Encodings.Web;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace MvcMovie.Controllers
{
    public class HelloWorldController : Controller
    {
        //
        // GET: /HelloWorld/
        public IActionResult Index()
        {
            return View();

        }

        //
        // GET: /HelloWorld/Welcome
        // Requires using System.Text.Encodings.Web
        // matches HelloWorld/Welcome/5?name=Rick because 
        // id matches to map route 
        public IActionResult Welcome(string name, int numTimes = 1)
        {
            ViewData["Message"] = "Hello " + name;
            ViewData["NumTimes"] = numTimes;

            return View();

        }
        // there is no requirement to add comment with endpoint
        // method name define endpoint
        public string Other()
        {
            return "This is the Other action method...";

        }
    }
}
